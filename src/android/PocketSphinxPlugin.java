import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

package org.apache.cordova.plugin;

public class PocketSphinxPlugin extends CordovaPlugin {

   public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
   }


    public void create() {

    }

    public void startListening() {

    }

    public void stopListening() {

    }

    public void disengage() {
        
    }
}