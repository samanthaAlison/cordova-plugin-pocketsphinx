//
//  Listener.m
//  SphinxLib
//
//  Created by Team Urban Science on 3/29/18.
//  Copyright © 2018 Team Urban Science. All rights reserved.
//

#import "Listener.h"
#include "sphinxbase/ad.h"
#include "pocketsphinx/pocketsphinx_internal.h"
//#import <AVFoundation/AVFAudio/AVAudioSession>

const size_t SAMPLE_SIZE = 2;
const size_t MAX_READ_SIZE = 2048 * SAMPLE_SIZE;         // # of frames multiplied by size of each frame in bytes
const double SAMPLE_RATE = 16000.00;

static size_t frameTotal = 0;

static OSStatus recordingCallback(void *inRefCon,
                                  AudioUnitRenderActionFlags *ioActionFlags,
                                  const AudioTimeStamp *inTimeStamp,
                                  UInt32 inBusNumber,
                                  UInt32 inNumberFrames,
                                  AudioBufferList *ioData) {
    
    // the data gets rendered here
    AudioBuffer buffer;
    
    // a variable where we check the status
    OSStatus status;
    
    /**
     This is the reference to the object who owns the callback.
     */
    Listener *listener = (__bridge Listener*) inRefCon;
    
    
    buffer.mDataByteSize = inNumberFrames * 2; // sample size
    buffer.mNumberChannels = 1; // one channel
    buffer.mData = malloc( inNumberFrames * 2 ); // buffer size
    
    // we put our buffer into a bufferlist array for rendering
    AudioBufferList bufferList;
    bufferList.mNumberBuffers = 1;
    bufferList.mBuffers[0] = buffer;
    
    
    frameTotal += inNumberFrames;
    // render input and check for error
    status = AudioUnitRender([listener audioComponent], ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, &bufferList);
    
    // process the bufferlist in the audio processor
    [listener processBuffer:&bufferList];
    
    // clean up the buffer
    free(bufferList.mBuffers[0].mData);
    
    return noErr;
}


@interface Listener ()
@property (readwrite) SpeechStateEnum speechState;
@property (readwrite) ps_decoder_t *decoder;
@property (readwrite) Hypothesis *hypothesis;
@property (readwrite) AVAudioEngine *engine;
@property (readwrite) bool psWaitingForSpeech;
@property (readwrite) bool inUtterance;

@property (readwrite) ListenerCallback listenerCallback;
@property (readwrite) dispatch_queue_t readingQueue;
@property (readwrite) bool micEnabled;




@end

@implementation Listener
@synthesize audioComponent, audioBuffer, gain, framesProcessed;

cmd_ln_t * config; 

- (id) init {
    if (!(self = [super init]))
        return nil;
    self.speechState = NOT_SPOKEN;
    self.engine = [[AVAudioEngine alloc] init];
    self.psWaitingForSpeech = false;
    self.micEnabled = false;
    self.inUtterance = false;
    framesProcessed = 0;
    return self;
}


- (void)enableListening:(ListenerCallback)callback {
    
    self.listenerCallback = callback;
    
    NSLog(@"%@", NSBundle.mainBundle);
    NSBundle *main = NSBundle.mainBundle;
    NSString *modelRoot = [main.resourcePath stringByAppendingString: @"/model/en-us/"];
    NSString *hmm = [modelRoot stringByAppendingString:@"en-us"];
    NSString *kws = [modelRoot stringByAppendingString:@"keyword.list"];
    NSString *dict = [modelRoot stringByAppendingString:@"cmudict-en-us.dict"];
    
    char *argv[] =  {"-hmm", [hmm UTF8String], "-kws", [kws UTF8String], "-dict", [dict UTF8String]};
    config  = cmd_ln_parse_r(NULL, ps_args(), 6, argv, 1);
    
    self.decoder = ps_init(config);
    
    OSStatus status;

 
    
    // Let's make the criteria for our audio component
    AudioComponentDescription description;
    description.componentFlags = 0;
    description.componentFlagsMask = 0;
    description.componentType = kAudioUnitType_Output;
    description.componentSubType = kAudioUnitSubType_RemoteIO;
    description.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Let's find it.
    AudioComponent component = AudioComponentFindNext(NULL, &description);
    
    
    
    status = AudioComponentInstanceNew(component, &audioComponent);
    
    UInt32 flag = 1;
    // Enable input from mic
    status = AudioUnitSetProperty(audioComponent,
                                              kAudioOutputUnitProperty_EnableIO,
                                              kAudioUnitScope_Input,
                                              kInputBus,
                                              &flag,
                                              sizeof(flag));
    
    if (noErr != status) { NSLog(@"Set input enabled error"); return; }

    flag = 0;
    
    // Disable playback from speakers.
    status = AudioUnitSetProperty(audioComponent,
                                  kAudioOutputUnitProperty_EnableIO, // use io
                                  kAudioUnitScope_Output, // scope to output
                                  kOutputBus, // select output bus (0)
                                  &flag, // set flag
                                  sizeof(flag));
    
    if (noErr != status) { NSLog(@"Set output disabled error"); return; }

    AudioStreamBasicDescription inputFormat;
    inputFormat.mFormatID = kAudioFormatLinearPCM;
    inputFormat.mFormatFlags = kAudioFormatFlagIsPacked | kAudioFormatFlagIsSignedInteger;
    inputFormat.mFramesPerPacket = 1;
    inputFormat.mSampleRate = SAMPLE_RATE;
    inputFormat.mChannelsPerFrame = 1;
    inputFormat.mBitsPerChannel = 16;
    inputFormat.mBytesPerPacket = 2;
    inputFormat.mBytesPerFrame = 2;
    
    
    // Format of input
    status = AudioUnitSetProperty(audioComponent,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Output,
                                  kInputBus,
                                  &inputFormat,
                                  sizeof(inputFormat));
    
    status = AudioUnitSetProperty(audioComponent,
                                  kAudioUnitProperty_StreamFormat,
                                  kAudioUnitScope_Input,
                                  kOutputBus,
                                  &inputFormat,
                                  sizeof(inputFormat));
    
    if (noErr != status) {
        NSLog(@"Set input format error");
        return;
        
    }

    
    AURenderCallbackStruct callbackStruct;
    callbackStruct.inputProc = recordingCallback;
    callbackStruct.inputProcRefCon = (__bridge void * _Nullable)(self);
    
    
    status = AudioUnitSetProperty(audioComponent,
                                  kAudioOutputUnitProperty_SetInputCallback,
                                  kAudioUnitScope_Global,
                                  kInputBus,
                                  &callbackStruct,
                                  sizeof(callbackStruct));
    if (noErr != status) {
        NSLog(@"Set input callback error");
        return;
        
    }

  
    flag = 0;
    
    status = AudioUnitSetProperty(audioComponent,
                                   kAudioUnitProperty_ShouldAllocateBuffer,
                                   kAudioUnitScope_Output,
                                   kInputBus,
                                   &flag,
                                   sizeof(flag));
              
              
    audioBuffer.mDataByteSize = 1024;
    audioBuffer.mNumberChannels = 1;
    audioBuffer.mData = malloc(1024);


    status = AudioUnitInitialize(audioComponent);
    
  
}

- (NSString *) getSpeechStateString {
    
    switch(self.speechState)
    {
    case SPOKEN:
        return @"SPOKEN";
        break;
    case NOT_SPOKEN:
        return @"NOT SPOKEN";
        break;
    case SPEAKING:
        return @"SPEAKING";
        break;
    default:
        return @"OOPS";
        break;
        
    }
    
}

- (bool) start_utt {

    if (ps_start_utt(self.decoder) >= 0) {
        self.psWaitingForSpeech = true;
        return true;
    }
    return false;
}
- (bool) end_utt {
    if (ps_end_utt(self.decoder) >= 0) {
        self.inUtterance = false;
        return true;
    }
    return false;

}

- (void) stopListening {
    if (self.micEnabled) {
        self.micEnabled = false;
        OSStatus status = AudioOutputUnitStop(audioComponent);

        if (self.psWaitingForSpeech || self.inUtterance) {

            [self end_utt];
            self.psWaitingForSpeech = false;
        }
    }
}

-(void) startListening {
    if (!(self.micEnabled)) {
        OSStatus status = AudioOutputUnitStart(audioComponent);
        self.micEnabled = true;
        if (self.psWaitingForSpeech) {
            [self end_utt];
            [self start_utt];
        } else {
            if (self.inUtterance) {
                [self end_utt];
            }
            [self start_utt];
        }
    }
}

-(void)processBuffer: (AudioBufferList*) audioBufferList
{
    if (self.micEnabled) {
    
    
        
        AudioBuffer sourceBuffer = audioBufferList->mBuffers[0];
        
        
        size_t sampleCount = sourceBuffer.mDataByteSize / SAMPLE_SIZE;
        
        
        framesProcessed = self.decoder->n_frame;
        
        
        if (sampleCount > 0 && (self.inUtterance || self.psWaitingForSpeech)) {
            
            int frameCount = ps_process_raw(self.decoder, (int16_t *)audioBuffer.mData, sampleCount, 0, 0);

            bool inSpeech = ps_get_in_speech(self.decoder);
            

            
            if (inSpeech && !self.inUtterance) {
                // We just starting talking again.
                self.inUtterance = true;
                // We've gone from having a started utterance to one that is micEnabled to voice.
                self.psWaitingForSpeech = false;

                NSLog(@"SphinxLib:\tStarting to listen");
            }
            if (!inSpeech && self.inUtterance) {
                NSLog(@"Ending utterance\n");
                // We've gone from talking to being silent. Time to process and start a new utterance.
                [self end_utt];
                int32_t bestGuessScore;
                const char * bestGuess = ps_get_hyp(self.decoder, &bestGuessScore);
                if (bestGuess != NULL) {
                    NSString * guessString = [[NSString alloc] initWithUTF8String:bestGuess];
                    Hypothesis * hypothesis = [[Hypothesis alloc] initWithGuess:guessString AndScore:bestGuessScore];
                    self.listenerCallback(hypothesis);
                    NSLog(@"SphinxLib:\tHere's my best guess: %s", bestGuess);
                    fflush(stdout);
                    
                }
                
                [self start_utt];
            
                
                NSLog(@"SphinxLib:\t Ready to start listening again.");
            }
        }
        
        
        // we check here if the input data byte size has changed
        if (audioBuffer.mDataByteSize != sourceBuffer.mDataByteSize) {
            // clear old buffer
            free(audioBuffer.mData);
            // assing new byte size and allocate them on mData
            audioBuffer.mDataByteSize = sourceBuffer.mDataByteSize;
            audioBuffer.mData = malloc(sourceBuffer.mDataByteSize);
        }
        
    
        
    
        
    }
    
}




- (BOOL) disable {

    ps_free(self.decoder);
    cmd_ln_free_r(config);
    return true;
}


@end
