//
//  Hypothesis.h
//  SphinxLib
//
//  Created by Team Urban Science on 3/29/18.
//  Copyright © 2018 Team Urban Science. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hypothesis : NSObject

@property (readonly) NSString *guess;
@property (readonly) int32_t score;


- (id) initWithGuess:(NSString *)guess AndScore:(int32_t)score;



@end
