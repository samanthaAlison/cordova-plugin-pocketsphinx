//
//  PocketSphinxPlugin.h
//  PocketSphinxPlugin
//
//  Created by Team Urban Science on 3/29/18.
//  Copyright © 2018 Team Urban Science. All rights reserved.
//

#import <Cordova/CDV.h>

// In this header, you should import all the public headers of your framework using statements like #import <SphinxLib/PublicHeader.h>
#import "Listener.h"
#import "Hypothesis.h"


@interface PocketSphinxPlugin : CDVPlugin 

@property (copy)   NSString* callbackId;

- (void) create:(CDVInvokedUrlCommand*)command;
- (void) stopListening:(CDVInvokedUrlCommand*)command;
- (void) startListening:(CDVInvokedUrlCommand*)command;
- (void) onHypothesisGet:(Hypothesis*)hypothesis;
- (void) stopWatch:(CDVInvokedUrlCommand*)command;


@end

