//
//  Listener.h
//  SphinxLib
//
//  Created by Team Urban Science on 3/29/18.
//  Copyright © 2018 Team Urban Science. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hypothesis.h"
#import <AVFoundation/AVAudioEngine.h>
#import <AVFoundation/AVAudioFormat.h>
#import <AVFoundation/AVAudioSession.h>
#import <AVFoundation/AVAudioMixerNode.h>
#import <AVFoundation/AVAudioConverter.h>
#import <AudioToolbox/AudioToolbox.h>

#import "pocketsphinx/pocketsphinx.h"

#define kInputBus 1
#define kOutputBus 0
// return max value for given values
#define max(a, b) (((a) > (b)) ? (a) : (b))
// return min value for given values
#define min(a, b) (((a) < (b)) ? (a) : (b))

typedef enum {

    NOT_SPOKEN = 1,
    SPEAKING = 2,
    SPOKEN = 3
    
} SpeechStateEnum;



typedef struct buffer_fragments_s {
    
    int16_t ** fragments;
    
    int16_t fragment_count;
    
    int16_t * remainder;
    int16_t remainder_size;
} buffer_fragments_t;


typedef struct audio_circular_buffer_s {
    
    
    
    int16_t * _buffer;
    size_t size;
    size_t head;
    size_t tail;
    
    int empty;
    int overwrite;
    
} audio_circular_buffer_t;


/*audio_circular_buffer_t * create_buffer(size_t size);
void push_data_to_buffer(audio_circular_buffer_t * acb, const int16_t * data, size_t length);
size_t pop_data_from_buffer(audio_circular_buffer_t * acb, int16_t * output, size_t max);
size_t space_left_in_buffer(audio_circular_buffer_t * acb);
size_t length_of_buffer (audio_circular_buffer_t * acb);*/


typedef void (^ListenerCallback)(Hypothesis*);

@interface Listener : NSObject
{
    AudioComponentInstance audioComponent;
    AudioBuffer audioBuffer;
    float gain;
    size_t framesProcessed;
}

@property (readonly) Hypothesis *hypothesis;
@property (readonly) SpeechStateEnum speechState;
@property (readonly) ps_decoder_t *decoder;
@property (readonly) AVAudioEngine *engine;
@property (readonly) ListenerCallback listenerCallback;
@property (readonly) dispatch_queue_t readingQueue;
@property (readonly) AudioComponentInstance audioComponent;
@property (readonly) AudioBuffer audioBuffer;
@property (nonatomic) float gain;
@property (readonly) size_t framesProcessed;
/**
 * True if the audioComponent is currently listening using the mic.
 */
@property (readonly) bool micEnabled;
/**
 * True if PocketSphinx is not in the middle of hearing speech (i.e. ps_start_utt has been called but ps_get_in_speech has only returned false since).
 */
@property (readonly) bool psWaitingForSpeech;
/**
 * True if PocketSphinx is in the middle of hearing speech (i.e. the most recent ps_get_in_speech call returned true but we haven't called ps_end_utt yet).
 */ 
@property (readonly) bool inUtterance;




- (void)processBuffer: (AudioBufferList*)audioBufferList;
- (NSString *) getSpeechStateString;
- (void)enableListening:(ListenerCallback)callback;
- (void)startListening;
- (void)stopListening;
- (bool) start_utt;
- (bool) end_utt;
- (id) init;
- (BOOL) disable;
@end

/*
audio_circular_buffer_t * create_buffer(size_t size) {
    audio_circular_buffer_t * acb = malloc(sizeof(audio_circular_buffer_t));
    acb->size = size;
    acb->head = 0;
    acb->tail = 0;
    acb->_buffer = malloc(sizeof(int16_t) * size);
    acb->empty = 1;
    acb->overwrite = 0;
    
    
    return acb;
}

void push_data_to_buffer(audio_circular_buffer_t * acb, const int16_t * data, size_t length)
{
    size_t space_left = space_left_in_buffer(acb);
    int overwriteWillOccur = 0;
    if (space_left < length)
    {
        overwriteWillOccur = 1;
    }
    
    
    size_t l = 0;
    while (length > l) {
        size_t tail_room = acb->size - acb->tail;
        size_t length_to_write = (tail_room < length - l) ? tail_room : length - l;
        memcpy(acb->_buffer + acb->tail, data, length_to_write);
        acb->tail += length_to_write;
        acb->tail = acb->tail % acb->size;
        l += length_to_write;
        
    }
    if (overwriteWillOccur == 1)
    {
        acb->overwrite = 1;
        acb->head = acb->tail;
    }
    if (acb->tail == acb->head)
    {
        acb->empty = 0;
    }
}
size_t pop_data_from_buffer(audio_circular_buffer_t * acb, int16_t * output, size_t max)
{
    
    size_t buffer_size = length_of_buffer(acb);
    
    size_t read_length = (buffer_size >= max) ? max : buffer_size;
    
    
    //output = malloc(sizeof(int16_t) * read_length);
    
    
    size_t length = 0;
    while (read_length > length) {
        // head_room is the amount of bytes between the head location and either the tail location
        // or the end of the buffer array, whichever of the two comes first.
        size_t head_room = (acb->head < acb->tail) ? acb->tail - acb->head : acb->size - acb->head;
        // If the amount of bytes we need to read in total is less than the the head_room, only
        // read the bytes we need.
        size_t length_to_read = (head_room < read_length - length) ? head_room : read_length - length;
        memcpy(&output[length], acb->_buffer + acb->head, length_to_read);
        // These bytes have been read, so move the head past them.
        acb->head += length_to_read;
        acb->head = acb->head % acb->size;
        
        // We only need to read read_length - length more bytes
        length += length_to_read;
        
    }
    if (acb->tail == acb->head)
    {
        acb->empty = 1;
    }
    return read_length;
}
size_t space_left_in_buffer(audio_circular_buffer_t * acb)
{
    
    if (acb->head == acb->tail)
    {
        if (acb->empty == 1) {
            return acb->size;
        }
        return 0;
    }
    return acb->size + acb->tail - acb->head;
}

size_t length_of_buffer (audio_circular_buffer_t * acb)
{
    return acb->size - space_left_in_buffer(acb);
}

/**#define kOutputBus 0
#define kInputBus 1
// ...
OSStatus status;
AudioComponentInstance audioUnit;
// Describe audio component
AudioComponentDescription desc;
desc.componentType = kAudioUnitType_Output;
desc.componentSubType = kAudioUnitSubType_RemoteIO;
desc.componentFlags = 0;
desc.componentFlagsMask = 0;
desc.componentManufacturer = kAudioUnitManufacturer_Apple;
// Get component
AudioComponent inputComponent = AudioComponentFindNext(NULL, &desc);
// Get audio units
status = AudioComponentInstanceNew(inputComponent, &audioUnit);
checkStatus(status);
// Enable IO for recording
UInt32 flag = 1;
status = AudioUnitSetProperty(audioUnit,
                              kAudioOutputUnitProperty_EnableIO,
                              kAudioUnitScope_Input,
                              kInputBus,
                              &flag,
                              sizeof(flag));
checkStatus(status);
// Enable IO for playback
status = AudioUnitSetProperty(audioUnit,
                              kAudioOutputUnitProperty_EnableIO,
                              kAudioUnitScope_Output,
                              kOutputBus,
                              &flag,
                              sizeof(flag));
checkStatus(status);
// Describe format
audioFormat.mSampleRate = 44100.00;
audioFormat.mFormatID = kAudioFormatLinearPCM;
audioFormat.mFormatFlags = kAudioFormatFlagIsSignedInteger | kAudioFormatFlagIsPacked;
audioFormat.mFramesPerPacket    = 1;
audioFormat.mChannelsPerFrame    = 1;
audioFormat.mBitsPerChannel = 16;
audioFormat.mBytesPerPacket = 2;
audioFormat.mBytesPerFrame = 2;
// Apply format
status = AudioUnitSetProperty(audioUnit,
                              kAudioUnitProperty_StreamFormat,
                              kAudioUnitScope_Output,
                              kInputBus,
                              &audioFormat,
                              sizeof(audioFormat));
checkStatus(status);
status = AudioUnitSetProperty(audioUnit,
                              kAudioUnitProperty_StreamFormat,
                              kAudioUnitScope_Input,
                              kOutputBus,
                              &audioFormat,
                              sizeof(audioFormat));
checkStatus(status);
// Set input callback
AURenderCallbackStruct callbackStruct;
callbackStruct.inputProc = recordingCallback;
callbackStruct.inputProcRefCon = self;
status = AudioUnitSetProperty(audioUnit,
                              kAudioOutputUnitProperty_SetInputCallback,
                              kAudioUnitScope_Global,
                              kInputBus,
                              &callbackStruct,
                              sizeof(callbackStruct));
checkStatus(status);
// Set output callback
callbackStruct.inputProc = playbackCallback;
callbackStruct.inputProcRefCon = self;
status = AudioUnitSetProperty(audioUnit,
                              kAudioUnitProperty_SetRenderCallback,
                              kAudioUnitScope_Global,
                              kOutputBus,
                              &callbackStruct,
                              sizeof(callbackStruct));
checkStatus(status);
// Disable buffer allocation for the recorder (optional - do this if we want to pass in our own)
flag = 0;
status = AudioUnitSetProperty(audioUnit,
                              kAudioUnitProperty_ShouldAllocateBuffer,
                              kAudioUnitScope_Output,
                              kInputBus,
                              &flag,
                              sizeof(flag));
// TODO: Allocate our own buffers if we want
// Initialise
status = AudioUnitInitialize(audioUnit);
checkStatus(status);
*/
