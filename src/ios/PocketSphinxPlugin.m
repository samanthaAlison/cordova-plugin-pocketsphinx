
#import "PocketSphinxPlugin.h" 

@interface PocketSphinxPlugin () 
@property (nonatomic, retain) Listener *listener;
@end

@implementation PocketSphinxPlugin 


- (void) create:(CDVInvokedUrlCommand*)command {

    self.listener = [[Listener alloc] init];
    
    [self.listener enableListening:^(Hypothesis *hypothesis) { [self onHypothesisGet: hypothesis]; }];

    self.callbackId = command.callbackId;
}

- (void) onHypothesisGet:(Hypothesis*)hypothesis {
    NSMutableDictionary *jsonObj = [[NSMutableDictionary alloc] init];
    [jsonObj setValue: hypothesis.guess forKey:@"guess"];
    [jsonObj setValue: [NSString stringWithFormat:@"%i", (int) hypothesis.score] forKey:@"score"];

    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:jsonObj];
    [result setKeepCallbackAsBool:1];
    [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
}

- (void) stopWatch:(CDVInvokedUrlCommand*)command {
    [self.listener disable];
}


- (void) stopListening:(CDVInvokedUrlCommand*)command {
    [self.listener stopListening];
}
- (void) startListening:(CDVInvokedUrlCommand*)command {
    [self.listener startListening];

}
@end
