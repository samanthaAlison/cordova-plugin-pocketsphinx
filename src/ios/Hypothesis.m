//
//  Hypothesis.m
//  SphinxLib
//
//  Created by Team Urban Science on 3/29/18.
//  Copyright © 2018 Team Urban Science. All rights reserved.
//

#import "Hypothesis.h"
@interface Hypothesis ()
@property (readwrite) NSString *guess;
@property (readwrite) int32_t score;

@end

@implementation Hypothesis


- (id) initWithGuess:(NSString *)guess AndScore:(int32_t)score {
    
    if (!(self = [super init]))
        return nil;
    self.guess = guess;
    self.score = score;
    return self;   
}


@end
