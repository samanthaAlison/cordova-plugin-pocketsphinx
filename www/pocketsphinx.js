var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');
var PocketSphinx = function(){

}

PocketSphinx.prototype = {
  initialize: function(successCallback, errorCallback){
    var pocketsphinx = this;
    var id = utils.createUUID();
    cordova.exec(successCallback, errorCallback, "PocketSphinxPlugin", "create", []);
    return id;
  },
  disengage: function() {

    cordova.exec(function() {}, function() {throw "Error disengaging pocketsphinx plugin"}, "PocketSphinxPlugin", "stopWatch", []);
  },
  startListening: function() {
    cordova.exec(function() {}, function() {throw "Error starting pocketsphinx listening"}, "PocketSphinxPlugin", "startListening", []);

  },
  stopListening: function() {
    cordova.exec(function() {}, function() {throw "Error stopping pocketsphinx listening"}, "PocketSphinxPlugin", "stopListening", []);

  }
}

module.exports = new PocketSphinx();
